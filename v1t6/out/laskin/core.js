// Compiled by ClojureScript 0.0-3269 {}
goog.provide('laskin.core');
goog.require('cljs.core');
laskin.core.dest = document.getElementById("tulos");
laskin.core.hae_numero = (function laskin$core$hae_numero(elem){
return parseInt(document.getElementById(elem).value);
});
laskin.core.plus = (function laskin$core$plus(){
return laskin.core.dest.innerHTML = (laskin.core.hae_numero.call(null,"eka") + laskin.core.hae_numero.call(null,"toka"));
});
goog.exportSymbol('laskin.core.plus', laskin.core.plus);
laskin.core.minus = (function laskin$core$minus(){
return laskin.core.dest.innerHTML = (laskin.core.hae_numero.call(null,"eka") - laskin.core.hae_numero.call(null,"toka"));
});
goog.exportSymbol('laskin.core.minus', laskin.core.minus);
laskin.core.kerto = (function laskin$core$kerto(){
return laskin.core.dest.innerHTML = (laskin.core.hae_numero.call(null,"eka") * laskin.core.hae_numero.call(null,"toka"));
});
goog.exportSymbol('laskin.core.kerto', laskin.core.kerto);
laskin.core.jako = (function laskin$core$jako(){
return laskin.core.dest.innerHTML = (laskin.core.hae_numero.call(null,"eka") / laskin.core.hae_numero.call(null,"toka"));
});
goog.exportSymbol('laskin.core.jako', laskin.core.jako);

//# sourceMappingURL=core.js.map
(ns laskin.corr
	(:require [clojure.browser.repl :as repl]))

#_(enable-console-print!)

(defonce conn
  (repl/connect "http://localhost:9000/repl")) 

(defn parse-from-html [elem]
	(js/parseInt (.getElementById js/document elem)))

(defn ^:export plus []
	(let [dest (.getElementById js/document "tulos")]
		(set! (.-innerHTML dest) 
			(+ (parse-from-html "eka")
			   (parse-from-html "toka")))))

(defn ^:export sika-sanoo []
	(.alert js/window (str "nöf nöf")))

(defn ^:export kana-sanoo []
	(.alert js/window (str "kot kot")))


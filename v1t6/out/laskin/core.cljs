(ns laskin.core)

(def dest (.getElementById js/document "tulos"))

(defn hae-numero [elem]
	(js/parseInt (.-value (.getElementById js/document elem))))

(defn ^:export plus []
	(set! (.-innerHTML dest)
		(+ (hae-numero "eka")
		   (hae-numero "toka"))))

(defn ^:export minus []
	(set! (.-innerHTML dest)
		(- (hae-numero "eka")
		   (hae-numero "toka"))))

(defn ^:export kerto []
	(set! (.-innerHTML dest)
		(* (hae-numero "eka")
		   (hae-numero "toka"))))

(defn ^:export jako []
	(set! (.-innerHTML dest)
		(/ (hae-numero "eka")
		   (hae-numero "toka"))))


// Compiled by ClojureScript 0.0-3269 {}
goog.provide('laskin.corr');
goog.require('cljs.core');
goog.require('clojure.browser.repl');
if(typeof laskin.corr.conn !== 'undefined'){
} else {
laskin.corr.conn = clojure.browser.repl.connect.call(null,"http://localhost:9000/repl");
}
laskin.corr.parse_from_html = (function laskin$corr$parse_from_html(elem){
return parseInt(document.getElementById(elem));
});
laskin.corr.plus = (function laskin$corr$plus(){
var dest = document.getElementById("tulos");
return dest.innerHTML = (laskin.corr.parse_from_html.call(null,"eka") + laskin.corr.parse_from_html.call(null,"toka"));
});
goog.exportSymbol('laskin.corr.plus', laskin.corr.plus);
laskin.corr.sika_sanoo = (function laskin$corr$sika_sanoo(){
return window.alert([cljs.core.str("n\u00F6f n\u00F6f")].join(''));
});
goog.exportSymbol('laskin.corr.sika_sanoo', laskin.corr.sika_sanoo);
laskin.corr.kana_sanoo = (function laskin$corr$kana_sanoo(){
return window.alert([cljs.core.str("kot kot")].join(''));
});
goog.exportSymbol('laskin.corr.kana_sanoo', laskin.corr.kana_sanoo);

//# sourceMappingURL=corr.js.map
// Compiled by ClojureScript 0.0-3269 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
cljs.repl.print_doc = (function cljs$repl$print_doc(m){
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4422__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4422__auto__)){
var ns = temp__4422__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__5447_5459 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__5448_5460 = null;
var count__5449_5461 = (0);
var i__5450_5462 = (0);
while(true){
if((i__5450_5462 < count__5449_5461)){
var f_5463 = cljs.core._nth.call(null,chunk__5448_5460,i__5450_5462);
cljs.core.println.call(null,"  ",f_5463);

var G__5464 = seq__5447_5459;
var G__5465 = chunk__5448_5460;
var G__5466 = count__5449_5461;
var G__5467 = (i__5450_5462 + (1));
seq__5447_5459 = G__5464;
chunk__5448_5460 = G__5465;
count__5449_5461 = G__5466;
i__5450_5462 = G__5467;
continue;
} else {
var temp__4422__auto___5468 = cljs.core.seq.call(null,seq__5447_5459);
if(temp__4422__auto___5468){
var seq__5447_5469__$1 = temp__4422__auto___5468;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__5447_5469__$1)){
var c__3730__auto___5470 = cljs.core.chunk_first.call(null,seq__5447_5469__$1);
var G__5471 = cljs.core.chunk_rest.call(null,seq__5447_5469__$1);
var G__5472 = c__3730__auto___5470;
var G__5473 = cljs.core.count.call(null,c__3730__auto___5470);
var G__5474 = (0);
seq__5447_5459 = G__5471;
chunk__5448_5460 = G__5472;
count__5449_5461 = G__5473;
i__5450_5462 = G__5474;
continue;
} else {
var f_5475 = cljs.core.first.call(null,seq__5447_5469__$1);
cljs.core.println.call(null,"  ",f_5475);

var G__5476 = cljs.core.next.call(null,seq__5447_5469__$1);
var G__5477 = null;
var G__5478 = (0);
var G__5479 = (0);
seq__5447_5459 = G__5476;
chunk__5448_5460 = G__5477;
count__5449_5461 = G__5478;
i__5450_5462 = G__5479;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_5480 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__3361__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__3361__auto__)){
return or__3361__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_5480);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_5480)))?cljs.core.second.call(null,arglists_5480):arglists_5480));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__5451 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__5452 = null;
var count__5453 = (0);
var i__5454 = (0);
while(true){
if((i__5454 < count__5453)){
var vec__5455 = cljs.core._nth.call(null,chunk__5452,i__5454);
var name = cljs.core.nth.call(null,vec__5455,(0),null);
var map__5456 = cljs.core.nth.call(null,vec__5455,(1),null);
var map__5456__$1 = ((cljs.core.seq_QMARK_.call(null,map__5456))?cljs.core.apply.call(null,cljs.core.hash_map,map__5456):map__5456);
var doc = cljs.core.get.call(null,map__5456__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__5456__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__5481 = seq__5451;
var G__5482 = chunk__5452;
var G__5483 = count__5453;
var G__5484 = (i__5454 + (1));
seq__5451 = G__5481;
chunk__5452 = G__5482;
count__5453 = G__5483;
i__5454 = G__5484;
continue;
} else {
var temp__4422__auto__ = cljs.core.seq.call(null,seq__5451);
if(temp__4422__auto__){
var seq__5451__$1 = temp__4422__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__5451__$1)){
var c__3730__auto__ = cljs.core.chunk_first.call(null,seq__5451__$1);
var G__5485 = cljs.core.chunk_rest.call(null,seq__5451__$1);
var G__5486 = c__3730__auto__;
var G__5487 = cljs.core.count.call(null,c__3730__auto__);
var G__5488 = (0);
seq__5451 = G__5485;
chunk__5452 = G__5486;
count__5453 = G__5487;
i__5454 = G__5488;
continue;
} else {
var vec__5457 = cljs.core.first.call(null,seq__5451__$1);
var name = cljs.core.nth.call(null,vec__5457,(0),null);
var map__5458 = cljs.core.nth.call(null,vec__5457,(1),null);
var map__5458__$1 = ((cljs.core.seq_QMARK_.call(null,map__5458))?cljs.core.apply.call(null,cljs.core.hash_map,map__5458):map__5458);
var doc = cljs.core.get.call(null,map__5458__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__5458__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__5489 = cljs.core.next.call(null,seq__5451__$1);
var G__5490 = null;
var G__5491 = (0);
var G__5492 = (0);
seq__5451 = G__5489;
chunk__5452 = G__5490;
count__5453 = G__5491;
i__5454 = G__5492;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}
});

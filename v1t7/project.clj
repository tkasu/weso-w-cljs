(defproject v1t7 "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.7.0-beta3"]
                 [org.clojure/clojurescript "0.0-3269"]]
  :plugins [[lein-cljsbuild "1.0.6"]]
  :cljsbuild {
              :builds [{
                        :source-paths ["src/perus_mooc/"]
                        :compiler {
                                   :output-to "resources/main.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]})

